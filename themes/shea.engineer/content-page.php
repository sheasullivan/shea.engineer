<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package _tk
 */
?>

<?php /*<div class="fullscreen background" style="background-image:url('<?php echo get_the_post_thumbnail_url( $post->ID, 'full' ); ?>');" >
		<div class="content-a">
			<div class="content-b">
				<p>Above fold content goes here</p>
			</div>
		</div>
</div> */ ?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-content">
				<?php the_content(); ?>
	</div><!-- .entry-content -->
</article><!-- #post-## -->
